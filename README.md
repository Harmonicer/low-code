# AnyLayout低代码平台
## 1. 项目简介
拖拽生成h5网页的低代码平台，内部集成element-ui组件，支持flex,absolute模式布局，一键导出/发布

## 2. 项目说明文档
### 2.1 教程视频
https://doc.gql.fit/example/one

### 2.2教程视频
https://www.bilibili.com/video/BV1PG4y1k7Gd?spm_id_from=333.1007.top_right_bar_window_default_collection.content.click

## 3. 项目Demo体验

http://doc.gql.fit

## 4. 本地启动项目步骤
```
npm install

npm run serve
```

