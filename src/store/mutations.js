export default {
  setCenterView(state, payload) {
    state.centerView = payload
  },
  setStep(state, payload) {
    state.step = payload
  },
  setCurrentCom(state, payload) {
    state.currentCom = payload
  },
  setAgentType(state, payload) {
    state.agentType = payload
  },
  setCurrentComWraper(state, payload) {
    state.currentComWraper = payload
  },
  setComWraperChild(state, payload) {
    state.currentComWraper.children = payload
  },
  setCurrentComFocus(state, payload) {
    state.currentCom.focus = payload
  },
  updateCurrentComChild(state, payload) {
    state.currentCom.children = payload
  },
  setIsNewCom(state, payload) {
    state.isNewCom = payload
  },

  setIsEdit(state, payload) {
    state.isEdit = payload
  },
  setBeginIndex(state, payload) {
    state.beginIndex = payload
  },
  setDragedComInfo(state, payload) {
    state.dragedComInfo = payload
  },
  resetCenterView(state) {
    state.centerView = []
  },
}
