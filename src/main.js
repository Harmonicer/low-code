import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import mitt from 'mitt'
import store from './store'
import ElementPlus from 'element-plus'
import Components from '@/components'
import 'element-plus/dist/index.css'
import '@/scss/global.scss'
import 'ant-design-vue/dist/reset.css'

const app = createApp(App)

//使用mitt作为事件总线
app.config.globalProperties.$bus = mitt()

app
  .use(Components) //全局挂载组件
  .use(store) //使用路由
  .use(router) //使用路由
  .use(ElementPlus) //引入elementui
  .mount('#app')
