//基础组件
import ImageCom from './basic/imageCom.vue'
import BtnCom from './basic/btnCom.vue'
import TextCom from './basic/textCom.vue'
import VideoCom from './basic/videoCom.vue'
import LinkCom from './basic/LinkCom.vue'
import FlexBox from './FlexBox.vue'
//属性模块
import BtnProps from './comProps/BtnProps.vue'
import ImageProps from './comProps/ImageProps.vue'
import LinkProps from './comProps/LinkProps.vue'
import VideoProps from './comProps/VideoProps.vue'
import TextProps from './comProps/TextProps.vue'
import FlexBoxProps from './comProps/FlexBoxProps.vue'
//样式模块
import BtnStyle from './comStyle/BtnStyle.vue'
import ImageStyle from './comStyle/ImageStyle.vue'
import LinkStyle from './comStyle/LinkStyle.vue'
import VideoStyle from './comStyle/VideoStyle.vue'
import TextStyle from './comStyle/TextStyle.vue'
import FlexBoxStyle from './comStyle/FlexBoxStyle.vue'
//设置公共属性
import colorChoose from './module/colorChoose.vue'
import numInput from './module/numInput.vue'
import selectCols from './module/selectCols.vue'
import selectGroup from './module/selectGroup.vue'
import switchBtn from './module/switchBtn.vue'
import txtInput from './module/txtInput.vue'

export default {
  install(app) {
    app
      .component('ImageCom', ImageCom)
      .component('BtnCom', BtnCom)
      .component('LinkCom', LinkCom)
      .component('TextCom', TextCom)
      .component('VideoCom', VideoCom)
      .component('FlexBox', FlexBox)
      .component('BtnProps', BtnProps)
      .component('ImageProps', ImageProps)
      .component('LinkProps', LinkProps)
      .component('VideoProps', VideoProps)
      .component('TextProps', TextProps)
      .component('FlexBoxProps', FlexBoxProps)
      .component('BtnStyle', BtnStyle)
      .component('ImageStyle', ImageStyle)
      .component('LinkStyle', LinkStyle)
      .component('VideoStyle', VideoStyle)
      .component('TextStyle', TextStyle)
      .component('FlexBoxStyle', FlexBoxStyle)
      .component('colorChoose', colorChoose)
      .component('numInput', numInput)
      .component('selectCols', selectCols)
      .component('selectGroup', selectGroup)
      .component('switchBtn', switchBtn)
      .component('txtInput', txtInput)
  },
}
