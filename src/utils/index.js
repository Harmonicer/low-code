import { nanoid } from 'nanoid'
import deepCopy from './deepCopy'
import commonData from '@/data/commonData'

/**
 * 输入组件名创建新组件
 * @param {object} objA
 * @param {object} objB
 * @returns {Array<comNode>}
 */
export const isObjEqual = function (objA, objB) {
  //获得两个对象的属性数组
  objA = objA || {}
  objB = objB || {}
  const aPorps = Object.keys(objA)
  const bPorps = Object.keys(objB)
  if (aPorps.length != bPorps.length) {
    return false
  }
  for (let i = 0; i < aPorps.length; i++) {
    const propName = aPorps[i]
    const type = typeof objA[propName]
    if (type == 'object') {
      if (!isObjEqual(objA[propName], objB[propName])) return false
    } else if (objA[propName] !== objB[propName]) {
      if (propName != 'focus') return false
      //如果是属性focus不一样就继续比较
    }
  }
  return true
}

/**
 * 输入组件名创建新组件
 * @param {string} comName
 * @param {boolean} isDouble
 * @returns {Array<comNode>}
 */

export const createNewCom = function (comName, isDouble = false) {
  const newCom = deepCopy(commonData[comName])
  newCom.id = nanoid(5)
  if (!isDouble) {
    return [newCom]
  } else {
    let cloneCom = deepCopy(newCom)
    cloneCom.id = nanoid(5)
    return [newCom, cloneCom]
  }
}
