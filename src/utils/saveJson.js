function saveJson(data, filename) {
    if (!data) {
        this.$message('界面为空！')
        return
    }
    if (!filename) {
        filename = 'json.json'
    }
    // 下载保存json文件
    var eleLink = document.createElement("a");
    eleLink.download = filename + '.json';
    eleLink.style.display = "none";
    // 字符内容转变成blob地址
    data = JSON.stringify(data, undefined, 4);
    var blob = new Blob([data], { type: "text/json" });
    eleLink.href = URL.createObjectURL(blob);
    // 触发点击
    document.body.appendChild(eleLink);
    eleLink.click();
    // 然后移除
    document.body.removeChild(eleLink);
}
export default saveJson