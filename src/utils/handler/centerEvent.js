import { nanoid } from 'nanoid'
import saveJson from '@/utils/saveJson'

export function eventHandler(eventArr, that) {
  for (let eName of eventArr) {
    switch (eName) {
      case 'isNew':
        isNew(that)
        break
      case 'Pointed':
        Pointed(that)
        break
      case 'addChildFlexBox':
        addChildFlexBox(that)
        break
      case 'dropCom':
        dropCom(that)
        break
      case 'dragStartEvent':
        dragStartEvent(that)
        break
      case 'dragEnter':
        dragEnter(that)
        break
      case 'backOff':
        backOff(that)
        break
      case 'importJSON':
        importJSON(that)
        break
      case 'reset':
        reset(that)
        break
      case 'save':
        save(that)
        break
      case 'deleteCom':
        deleteCom(that)
        break
      case 'changeCanva':
        changeCanva(that)
        break
      case 'outputJson':
        outputJson(that)
        break
      case 'forward':
        forward(that)
    }
  }
}
//判断是否是新添加组件
function isNew(that) {
  that.$bus.on('isNew', () => {
    that.isNew = true
  })
}

//选中组件
function Pointed(that) {
  that.$bus.on('Pointed', ({ v, n, i }) => {
    that.dataTransfer(v, n, i)
    that.focus(n)
  })
}
//增加子弹性盒子
function addChildFlexBox(that) {
  that.$bus.on('addChildFlexBox', (view) => {
    that.addChildFlexBox(view)
  })
}
//放入组件
function dropCom(that) {
  that.$bus.on('dropCom', ({ e, view }) => {
    that.drop(e, view)
  })
}
//开始画布内拖拽组件
function dragStartEvent(that) {
  that.$bus.on('dragStartEvent', ({ v, n, i }) => {
    that.dataTransfer(v, n, i)
  })
}
//拖动组件进入盒子
function dragEnter(that) {
  that.$bus.on('dragEnter', (view) => {
    that.focus(view)
  })
}
//后退
function backOff(that) {
  that.$bus.on('backOff', () => {
    that.step--
    that.isRoll = true
    that.dataList = JSON.parse(sessionStorage.getItem(String(that.step)))
  })
}
//导入json文件
function importJSON(that) {
  that.$bus.on('importJSON', (data) => {
    that.dataList = data
    that.$bus.emit('cleanViews')
    that.$message.success('导入成功')
  })
}
//前进
function forward(that) {
  that.$bus.on('forward', () => {
    that.step++
    that.isRoll = true
    that.dataList = JSON.parse(sessionStorage.getItem(String(that.step)))
  })
}
//重置
function reset(that) {
  that.$bus.on('reset', () => {
    for (let i = that.begin; i < that.step; i++) {
      sessionStorage.removeItem(String(i))
    }
    that.step = 0
    that.begin = 1
    that.dataList = []
    that.$message.success('已重置界面')
  })
}
//保存
function save(that) {
  that.$bus.on('save', () => {
    localStorage.setItem('saveJson', JSON.stringify(that.dataList))
    that.$message.success('保存成功')
  })
}
//删除组件
function deleteCom(that) {
  that.$bus.on('deleteCom', () => {
    that.currentView.splice(that.currentIndex, 1)
    that.$bus.emit('cleanViews')
  })
}
//更改画布模式
function changeCanva(that) {
  that.$bus.on('changeCanva', (isPC) => {
    that.isPC = isPC
  })
}
//保存Json文件
function outputJson(that) {
  that.$bus.on('outputJson', () => {
    console.log(that.dataList)
    saveJson(that.dataList, nanoid(5))
  })
}
