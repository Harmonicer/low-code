import { computed } from 'vue'
import { mapState } from 'vuex'

/**
 * 映射store对应的属性
 * @param stateArray
 * @returns {[porpName:string]:ComputedRefImp,...}
 */
export default function useGetState($store, stateArray) {
  const mapData = {}
  const mappers = mapState(stateArray)
  Object.keys(mappers).forEach((key) => {
    mapData[key] = computed(mappers[key].bind({ $store }))
  })
  return mapData
}
